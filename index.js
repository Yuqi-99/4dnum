/**
 * @format
 */
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import {store} from './app/store';
import {Provider as ReduxProvider} from 'react-redux';
import React from 'react';
import i18n from './i18n/i18n';

const ReduxApp = () => (
  <ReduxProvider store={store}>
    <App />
  </ReduxProvider>
);

AppRegistry.registerComponent(appName, () => ReduxApp);
