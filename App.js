import React from "react";
import { NativeBaseProvider} from "native-base";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { Platform, SafeAreaView, ScrollView } from "react-native";
import ResultsPage from "./Pages/resultsPage";
import Header from "./component/Header";
import NavigationFooter from "./component/NavigationFooter";
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import CheckTicketPage from './Pages/checkTicketPage';
import StatisticsPage from "./Pages/statisticsPage";

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <NativeBaseProvider>
        <SafeAreaView height={Platform.OS === 'android'? '9%':'14%'}>
          <Header />
        </SafeAreaView>
          <SafeAreaProvider>
            {/* <SafeAreaView >
              <ScrollView> */}
                <Stack.Navigator screenOptions={{headerShown: false}}>
                  <Stack.Screen name="Lottery Result" component={ResultsPage} />
                  <Stack.Screen name="Check Ticket" component={CheckTicketPage} />
                  <Stack.Screen name="Statistics" component={StatisticsPage} />
                </Stack.Navigator> 
                {/* <PrizePage /> */}
                {/* <CheckTicketPage /> */}
              {/* </ScrollView>
            </SafeAreaView> */}
          </SafeAreaProvider>
        <SafeAreaView>
          <NavigationFooter />
        </SafeAreaView>
      </NativeBaseProvider>
    </NavigationContainer>
  );
}