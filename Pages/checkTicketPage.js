import React from 'react';
import {
  NativeBaseProvider,
  Box,
  Flex,
  Text,
  Grid,
  Input,
  Button,
} from 'native-base';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Platform, StatusBar, SafeAreaView, ScrollView} from 'react-native';
import {useState} from 'react';
import TableCheck from '../component/TableCheck';
import LinearGradient from 'react-native-linear-gradient';

export default function CheckTicketPage() {
  // const [show, setShow] = React.useState(false);
  // const handleClick = () => setShow(!show);

  return (
    <SafeAreaView>
      <ScrollView backgroundColor="black">
        <Flex
          backgroundColor="gray.800"
          direction="column"
          paddingX="1.5"
          height="100%">
          <Flex
            direction="row"
            width="100%"
            paddingY="2"
            justifyContent="space-around"
            height="8%">
            <Input
              borderColor="black"
              backgroundColor="black"
              color="white"
              width="75%"
              borderRadius="7px"
              placeholder="Search"
              _text={{fontSize: '16px'}}
            />
            {/* <Button 
              backgroundColor='#2B5F75' 
              borderRadius='7px'
              _text={{color:'white', fontWeight:'bold', fontSize:'16px'}}
            >
              Search
            </Button> */}
            <LinearGradient
              colors={['#308192', '#2B4F6A', '#22264B']}
              justifyContent="center"
              width="22%"
              style={{borderRadius: 7}}>
              <Text
                color="white"
                fontWeight="bold"
                fontSize="15px"
                textAlign="center">
                Search
              </Text>
            </LinearGradient>
          </Flex>
          <Text color="gray.500" fontSize="12px" paddingX="2" paddingBottom="2">
            Enter 3 digit or 6 digit number
          </Text>

          <Flex flex="1" direction="row" paddingY="3" backgroundColor="#4A5567">
            <Text
              color="white"
              fontSize="13px"
              fontWeight="bold"
              flex="1.8"
              paddingX="1">
              DATE
            </Text>
            <Text
              color="white"
              fontSize="13px"
              fontWeight="bold"
              flex="1.2"
              paddingX="1">
              NUMBER
            </Text>
            <Text
              color="white"
              fontSize="13px"
              fontWeight="bold"
              flex="2.5"
              paddingX="1">
              RESULT
            </Text>
            <Text
              color="white"
              fontSize="13px"
              fontWeight="bold"
              flex="1.5"
              paddingX="1">
              ACTION
            </Text>
          </Flex>

          <TableCheck
            resultColor={true}
            date="2022-08-16"
            num="919628"
            result="3rd prize"
            action="ACTION"
          />
          <TableCheck
            resultColor={false}
            date="2022-08-16"
            num="999"
            result="Lose"
            action="ACTION"
          />
          <TableCheck
            resultColor={true}
            date="2022-08-16"
            num="331584"
            result="Side prize 1st prize"
            action="ACTION"
          />
          <TableCheck
            resultColor={true}
            date="2022-08-16"
            num="284"
            result="3 page numbers"
            action="ACTION"
          />
          <TableCheck
            resultColor={true}
            date="2022-08-16"
            num="331583"
            result="1st prize"
            action="ACTION"
          />
          <TableCheck
            resultColor={false}
            date="2022-08-16"
            num="122347"
            result="Lose"
            action="ACTION"
          />
          <TableCheck
            resultColor={false}
            date="2022-08-16"
            num="000"
            result="Lose"
            action="ACTION"
          />
          <TableCheck
            resultColor={false}
            date="2022-08-16"
            num="989899"
            result="Lose"
            action="ACTION"
          />
          <TableCheck
            resultColor={true}
            date="2022-08-16"
            num="295954"
            result="5th prize"
            action="ACTION"
          />
          <TableCheck
            resultColor={false}
            date="2022-08-16"
            num="123"
            result="Lose"
            action="ACTION"
          />

          <Button
            marginTop="3%"
            marginBottom="3%"
            backgroundColor="#E53E3E"
            _text={{fontWeight: 'bold', fontSize: '16px'}}>
            Clear All
          </Button>
        </Flex>
      </ScrollView>
    </SafeAreaView>
  );
}

//['#308192', '#2B4F6A', '#22264B']
