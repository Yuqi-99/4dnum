import React from 'react';
import {
  NativeBaseProvider,
  Box,
  Flex,
  Text,
  Grid,
  Input,
  Button,
  useSafeArea,
  Pressable,
} from 'native-base';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Platform, StatusBar, SafeAreaView, ScrollView} from 'react-native';
import {useState} from 'react';
import TableCheck from '../component/TableCheck';
import LinearGradient from 'react-native-linear-gradient';

export default function StatisticsPage() {
  const [sampleNumber, setSampleNumber] = useState(null);

  const getRandomNumber = () => {
    // let min = 0;
    // let max = 999;
    // setSampleNumber(Math.round(Math.random() * (max - min) + min))
    // setSampleNumber( Math.random().toString().concat("0".repeat(3)).substr(2,3) );
    setSampleNumber(Math.random().toString().substr(2, 3));
  };

  return (
    <Flex
      backgroundColor="gray.800"
      direction="column"
      paddingX="1.5"
      height="100%"
      justifyContent="center"
      alignItems="center">
      <Box
        backgroundColor="black"
        width="50%"
        justifyContent="center"
        alignItems="center"
        marginBottom="10%"
        height="12%">
        <Text color="white" fontSize="5xl">
          {sampleNumber}
        </Text>
      </Box>
      <LinearGradient
        colors={['#308192', '#2B4F6A', '#22264B']}
        justifyContent="center"
        alignItems="center"
        width="30%"
        height="7%"
        style={{borderRadius: 7}}>
        <Pressable
          onPress={() => {
            getRandomNumber();
          }}>
          <Text
            fontWeight="bold"
            fontSize="xl"
            color="white"
            textAlign="center">
            Spin
          </Text>
        </Pressable>
      </LinearGradient>
    </Flex>
  );
}
