import React from 'react';
import {NativeBaseProvider, Box, Flex, Text, HStack, View} from 'native-base';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Platform, StatusBar, SafeAreaView, ScrollView} from 'react-native';
import PrizeContainer from '../component/PrizeContainer';
import PrizeTitle from '../component/PrizeTitle';
import Number from '../component/Number';
import {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {getResults} from '../api/resources';
import {setResults} from '../features/resultSlice';
import useApi from '../hook/useApi';
import uuid from 'react-native-uuid';
import {useTranslation} from 'react-i18next';
import '../i18n/i18n';

function prizeList(data) {
  return data?.win_numbers.map(winNumber => {
    return <Text key={uuid.v4()}>{winNumber} </Text>;
  });
}

export default function ResultsPage() {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const results = useSelector(state => state.result.value);
  const {selectedDrawDate} = useSelector(state => state.result);

  const getResultsApi = useApi(getResults);

  useEffect(() => {
    getResultsApi.request(selectedDrawDate);
  }, [selectedDrawDate]);

  useEffect(() => {
    console.log(getResultsApi.data?.data);
    if (!getResultsApi.loading && getResultsApi.data) {
      dispatch(setResults(getResultsApi.data?.data));
    }
  }, [getResultsApi.loading, getResultsApi.data]);

  return (
    <>
      <SafeAreaView>
        <ScrollView backgroundColor="black">
          {!getResultsApi.loading && results ? (
            <Flex backgroundColor="black" paddingX="1.5" height="100%">
              {/* <Container /> */}
              {/* first row */}
              <Flex direction="row">
                <PrizeTitle title={t('First Prize')} />
                {/* <PrizeTitle title="1st Prize"/> */}
                <PrizeTitle title={t("last2Digits")} />
              </Flex>
              <Flex direction="row">
                <Number
                  isTwoColumn={true}
                  size={true}
                  alignLeftRight={false}
                  right={false}
                  left={false}
                  number={prizeList(results[0]?.results[0])}
                  number2={prizeList(results[0]?.results[5])}
                />
              </Flex>
              {/* second row */}
              <Flex direction="row">
                <PrizeTitle title={t("Last 3 number")} />
                <PrizeTitle title={t("First 3 number")} />
              </Flex>
              <Flex direction="row">
                <Number
                  isTwoColumn={false}
                  right={true}
                  left={false}
                  number={prizeList(results[0]?.results[6])}
                  // number2="876"
                />
                <Number
                  isTwoColumn={false}
                  right={true}
                  left={false}
                  number2={prizeList(results[0]?.results[7])}
                  // number2="300"
                />
              </Flex>
              {/* third row */}
              <Flex direction="row">
                <PrizeTitle width={true} title={t("Side Prize")} />
              </Flex>
              <Flex
                direction="row"
                backgroundColor="white"
                justifyContent="center"
                alignItems="center"
                height="60px"
                marginBottom="2">
                <Text fontSize="25px" fontWeight="extrabold" textAlign="center">
                  {prizeList(results[0]?.results[8])}
                </Text>
              </Flex>

              {/* fourth row */}
              <PrizeTitle width={true} title={t("2nd Prize")} />
              <PrizeContainer number={prizeList(results[0]?.results[1])} />

              {/* fifth row */}

              <PrizeTitle width={true} title={t("3rd Prize")} />
              <PrizeContainer number={prizeList(results[0]?.results[2])} />

              {/* sixth row */}

              <PrizeTitle width={true} title={t("4th Prize")}/>
              <PrizeContainer number={prizeList(results[0]?.results[3])} />

              {/* seventh row */}

              <PrizeTitle width={true} title={t("5th Prize")} />
              <PrizeContainer number={prizeList(results[0]?.results[4])} />
            </Flex>
          ) : (
            <Text>Loading...</Text>
          )}
        </ScrollView>
      </SafeAreaView>
    </>
  );
};
