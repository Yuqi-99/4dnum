import axios from 'axios';
import {REACT_APP_API_BASE_URL} from '../utils/constants'

export const axiosPublic = axios.create({
  baseURL: REACT_APP_API_BASE_URL,
});
