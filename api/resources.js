import { axiosPublic } from './axios';
import { DEFAULT_LANGUAGE } from '../utils/constants';

// API endpoint
const DRAW_LIST_URL = 'drawList';
const RESULT_URL = 'result';

// GET
const getDrawList = (site = DEFAULT_LANGUAGE) =>
    axiosPublic.get(`/${DRAW_LIST_URL}/${site}`);

const getResults = (date) =>
    axiosPublic.get(`/${RESULT_URL}/${date}`);

  export {
    getDrawList,
    getResults,
  };
  
