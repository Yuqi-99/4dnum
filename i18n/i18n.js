import i18n from 'i18next';
import {i18nextReactNativeLanguageDetector as LanguageDetector} from 'i18next-react-native-language-detector';
import i18next from 'i18next';
import i18nextReactNative from 'i18next-react-native-language-detector';
import {initReactI18next} from 'react-i18next';
import {AsyncStorageBackend as Backend} from 'react-native-i18next-backend';
import locale from 'react-native-locale-detector';
import thai from './th.json';
import eng from './en.json';
import chinese from './cn.json';

export const LANGUAGE_STOGRAGE_KEY = '@APP:languageCode';

i18next.use(initReactI18next).init({
  lng: 'th',
  fallbackLng:'th',
  resources: {
    th: {translation:thai,},
    en: {translation:eng,},
    cn: {translation:chinese,},
  },
  react: {
    useSuspense: false,
  },
  interpolation: {
    escapeValue: false,
  },
});

export default i18next;
