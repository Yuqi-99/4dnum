import {configureStore} from '@reduxjs/toolkit';

import resultReducer from '../features/resultSlice';
import languageReducer from '../features/languageSlice';

export const store = configureStore({
  reducer: {
    result: resultReducer,
    language: languageReducer,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});
