import React from 'react';
import {NativeBaseProvider, Box, Flex, Text, Spacer, HStack} from 'native-base';

const Number = ({
  isTwoColumn,
  size,
  number,
  number2,
  alignLeftRight,
  right,
  left,
  results,
}) => {
  return (
    <>
      {isTwoColumn ? (
        //1
        <Flex
          direction="row"
          justifyContent="center"
          alignItems="center"
          width="100%"
          height="60px"
          backgroundColor="white"
          marginBottom="2">
          <Box width="50%" alignSelf="center">
            <Text
              color="black"
              fontSize={size ? '35px' : '25px'}
              fontWeight="extrabold"
              textAlign={alignLeftRight ? 'right' : 'center'}
              marginRight={right ? '15px' : '0px'}>
              {number}
            </Text>
          </Box>
          <Box width="50%" alignSelf="center">
            <Text
              color="black"
              fontSize={size ? '35px' : '25px'}
              fontWeight="extrabold"
              textAlign={alignLeftRight ? 'left' : 'center'}
              marginRight={right ? '15px' : '0px'}>
              {number2}
            </Text>
          </Box>
        </Flex>
      ) : (
        //2
        <Flex
          direction="row"
          justifyContent="center"
          alignItems="center"
          width="50%"
          height="60px"
          backgroundColor="white"
          marginBottom="2">
          <Box alignSelf="center">
            <Text fontSize="30px" fontWeight="extrabold" textAlign="center">
              {number}
            </Text>
          </Box>
          <Box alignSelf="center">
            <Text fontSize="30px" fontWeight="extrabold" textAlign="center">
              {number2}
            </Text>
          </Box>
        </Flex>
      )}
    </>
  );
};

export default Number;

//width={width? '100%' : '50%'}
