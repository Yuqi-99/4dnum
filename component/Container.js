import React from 'react';
import {NativeBaseProvider, Box, Flex} from 'native-base';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Platform, StatusBar} from 'react-native';

const Container = () => {
  return (
    <Flex
      direction="row"
      width="100%"
      backgroundColor="#0C007F"
      justifyContent="space-around">
      <Flex direction="column" alignItems="center" width="50%">
        <Box
          alignSelf="center"
          _text={{color: 'white', fontSize: '20px', fontWeight: 'extrabold'}}>
          1st Prize
        </Box>
        <Box
          alignSelf="center"
          backgroundColor="white"
          width="100%"
          _text={{
            fontSize: '35px',
            fontWeight: 'extrabold',
            textAlign: 'center',
          }}>
          331583
        </Box>
      </Flex>

      <Flex direction="column" alignItems="center" width="50%">
        <Box
          alignSelf="center"
          _text={{color: 'white', fontSize: '20px', fontWeight: 'extrabold'}}>
          Last 2 digits
        </Box>
        <Box
          alignSelf="center"
          backgroundColor="white"
          width="100%"
          _text={{
            fontSize: '35px',
            fontWeight: 'extrabold',
            textAlign: 'center',
          }}>
          42
        </Box>
      </Flex>
    </Flex>
  );
};

export default Container;
