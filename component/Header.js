import {Box, CheckIcon, Flex, Select, Text} from 'native-base';
import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {getDrawList} from '../api/resources';
import {setDrawList, setSelectedDrawDate} from '../features/resultSlice';
import useApi from '../hook/useApi';
import LanguagesSwitcher from './LanguagesSwitcher';

function renderDayList(arr) {
  return arr.map(day => {
    return (
      <Select.Item key={day} value={day} label={day}>
        {day}
      </Select.Item>
    );
  });
}

const Header = ({}) => {
  const dispatch = useDispatch();
  const {drawList} = useSelector(state => state.result);

  const getDrawListApi = useApi(getDrawList);

  const handleDateChange = e => {
    dispatch(setSelectedDrawDate(e));
  };

  useEffect(() => {
    getDrawListApi.request();
  }, []);

  useEffect(() => {
    // console.log(getDrawListApi.loading)
    // console.log(getDrawListApi.data)
    if (!getDrawListApi.loading && getDrawListApi.data) {
      dispatch(setDrawList(getDrawListApi.data?.data));
      dispatch(setSelectedDrawDate(getDrawListApi.data?.data[0]));
    }
  }, [getDrawListApi.loading, getDrawListApi.data]);

  return (
    <Flex
      direction="row"
      justifyContent="space-around"
      alignItems="center"
      flex="1"
      backgroundColor="gray.800">
      <Box
        width="45px"
        height="45px"
        backgroundColor="#1A365D"
        justifyContent="center"
        alignItems="center"
        borderColor="white"
        borderWidth="1"
        borderRadius="7"
        _text={{color: 'white', fontWeight: 'bold'}}>
        NUM
      </Box>
      <Flex>
        <Select
          color="white"
          fontWeight="bold"
          fontSize="14px"
          maxMenuHeight="100"
          menuPlacement="auto"
          backgroundColor="black"
          borderColor="black"
          minWidth="180"
          minHeight="45px"
          accessibilityLabel="Date"
          placeholder={drawList[0]}
          _selectedItem={{
            endIcon: <CheckIcon size="5" />,
          }}
          mt={1}
          onValueChange={e => handleDateChange(e)}>
          {!getDrawListApi.loading && drawList ? (
            renderDayList(drawList)
          ) : (
            <Text>Loading...</Text>
          )}
        </Select>
      </Flex>
      <LanguagesSwitcher />
    </Flex>
  );
};

export default Header;
