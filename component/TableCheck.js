import React from 'react';
import {
  Box,
  Flex,
  Text,
  Grid,
  Input,
  IconButton,
  DeleteIcon,
} from 'native-base';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Platform, StatusBar, SafeAreaView, ScrollView} from 'react-native';
import {useState} from 'react';
import {RiDeleteBinLine} from 'react-icons/ri';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const TableCheck = ({date, num, result, action, resultColor}) => {
  // const [show, setShow] = React.useState(false);
  // const handleClick = () => setShow(!show);
  return (
    <Flex
      flex="1"
      direction="row"
      paddingY="3"
      backgroundColor="black"
      borderBottomWidth="1"
      borderColor="#1C232D">
      <Text
        color="white"
        fontSize="14px"
        fontWeight="bold"
        flex="1.8"
        paddingX="1">
        {date}
      </Text>
      <Text
        color="white"
        fontSize="14px"
        fontWeight="bold"
        flex="1.2"
        paddingX="1">
        {num}
      </Text>
      <Text
        color={resultColor ? '#37A169' : '#E53E3E'}
        fontSize="15px"
        fontWeight="bold"
        flex="2.5"
        paddingX="1">
        {result}
      </Text>
      <Flex flex="1.5" justifyContent="center">
        <IconButton maxHeight="3" maxWidth="50px">
          <DeleteIcon color="red.500" alignself="center" />
        </IconButton>
        {/* <MaterialIcon name="delete"></MaterialIcon> */}
      </Flex>
    </Flex>
  );
};

export default TableCheck;
