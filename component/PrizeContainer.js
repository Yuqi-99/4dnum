import React from 'react';
import {Wrap, Box, Flex, Text, HStack, Center} from 'native-base';

const PrizeContainer = ({number}) => {
  return (
    <Flex direction="row" width="100%" backgroundColor="white" marginBottom="2">
      <Box width="100%">
        <Flex
          direction="row"
          width="80%"
          alignSelf="center"
          justifyContent="center"
          alignItems="center"
          flexWrap="wrap"
          paddingY="1">
          <Text
            color="black"
            fontSize="22px"
            fontWeight="extrabold"
            paddingX="1.5"
            paddingY="0.5"
            textAlign="center">
            {number}
          </Text>
        </Flex>
      </Box>
    </Flex>
  );
};

export default PrizeContainer;
