import React from 'react';
import {
  NativeBaseProvider,
  CheckIcon,
  Box,
  Flex,
  Select,
  Text,
} from 'native-base';
import {useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {setLanguage} from '../features/languageSlice';
import {useTranslation} from 'react-i18next';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {LANGUAGE_STOGRAGE_KEY} from '../i18n/i18n';
import '../i18n/i18n';

const LanguagesSwitcher = () => {
  const dispatch = useDispatch();
  const language = useSelector(state => state.language);
  console.log(language);
  const {t, i18n} = useTranslation();
  const onLanguageChange = lang => {
    i18n
      .changeLanguage(lang)
      .then(() => {
        dispatch(setLanguage(lang));
        AsyncStorage.setItem(LANGUAGE_STOGRAGE_KEY, lang);
      })
      .catch(err => console.log(err));
  };
  console.log(i18n.language);
  // const getSavedLanguage = async () => {
  //   try {
  //     const lang = await AsyncStorage.getItem(LANGUAGE_STOGRAGE_KEY);
  //     if (lang) {
  //       dispatch(setLanguage(lang));
  //       i18n.changeLanguage(lang);
  //     }
  //   } catch (err) {
  //     console.log(err);
  //   }
  // };

  // useEffect(() => {
  //   getSavedLanguage();
  // }, []);

  return (
    <Flex>
      <Select
        color="white"
        fontWeight="bold"
        fontSize="14px"
        backgroundColor="black"
        borderColor="black"
        minWidth="130"
        minHeight="45px"
        placeholder="แบบไทย"
        mt={1}
        onValueChange={(value) => onLanguageChange(value)}>
        <Select.Item
          label="แบบไทย"
          value='th'
          // onPress={() => onLanguageChange('th')}
        />
        <Select.Item
          label="English"
          value='en'
          // onPress={() => onLanguageChange('en')}
        />
        <Select.Item
          label="中文"
          value='cn'
          // onPress={() => onLanguageChange('cn')}
        />
        {/* {renderLanguageOptions} */}
      </Select>
    </Flex>
  );
};

export default LanguagesSwitcher;


