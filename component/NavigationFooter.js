import React from 'react';
import {
  CheckIcon,
  Box,
  Text,
  Center,
  HStack,
  Pressable,
  Flex,
  Button,
} from 'native-base';
import {Platform, StatusBar, SafeAreaView, ScrollView} from 'react-native';
import {useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import LinearGradient from 'react-native-linear-gradient';
import {useTranslation} from 'react-i18next';

const NavigationFooter = ({}) => {
  const [selected, setSelected] = useState(1);
  const navigation = useNavigation();
  const {t} = useTranslation();

  return (
    <LinearGradient colors={['#308192', '#2B4F6A', '#22264B']}>
      <Flex
        direction="row"
        width="100%"
        height="50px"
        justifyContent="center"
        alignItems="center">
        <Button
          cursor="pointer"
          borderRadius="0"
          backgroundColor={selected === 1 ? '#222047' : 'transparent'}
          py="3.5"
          flex={1}
          onPress={() => {
            setSelected(1);
            navigation.navigate('Lottery Result');
          }}>
          <Center>
            <Text color="white" fontSize="15" fontWeight="bold">
              {t('lotteryResults')}
            </Text>
          </Center>
        </Button>
        <Button
          cursor="pointer"
          borderRadius="0"
          backgroundColor={selected === 2 ? '#222047' : 'transparent'}
          py="3.5"
          flex={1}
          onPress={() => {
            setSelected(2);
            navigation.navigate('Check Ticket');
          }}>
          <Center>
            <Text color="white" fontSize="15" fontWeight="bold">
              {t('checkTicket')}
            </Text>
          </Center>
        </Button>
        <Button
          cursor="pointer"
          borderRadius="0"
          backgroundColor={selected === 3 ? '#222047' : 'transparent'}
          py="3.5"
          flex={1}
          onPress={() => {
            setSelected(3);
            navigation.navigate('Statistics');
          }}>
          <Center>
            <Text color="white" fontSize="15" fontWeight="bold">
              {t('statistics')}
            </Text>
          </Center>
        </Button>
      </Flex>
    </LinearGradient>
  );
};

export default NavigationFooter;

{
  /* <Pressable flex='1' borderRadius='none'>
                <Text textAlign='center' color='white' fontWeight='bold'>Lottery results</Text>
            </Pressable >
            <Pressable flex='1' borderRadius='none'>
                <Text textAlign='center' color='white' fontWeight='bold'>Check ticket</Text>
            </Pressable >
            <Pressable  flex='1' borderRadius='none'>
                <Text textAlign='center' color='white' fontWeight='bold'>Statics</Text>
            </Pressable > */
}

//colors={['#308192', '#2B4F6A', '#22264B']}
