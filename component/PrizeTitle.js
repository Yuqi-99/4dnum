import React from 'react';
import {NativeBaseProvider, Box, Flex} from 'native-base';

const PrizeTitle = ({width, title}) => {
  return (
    <Flex
      direction="row"
      width={width ? '100%' : '50%'}
      backgroundColor="#0C007F"
      justifyContent="space-around"
      paddingY="1">
      <Flex direction="column" alignItems="center">
        <Box
          alignSelf="center"
          _text={{color: 'white', fontSize: '20px', fontWeight: 'extrabold'}}>
          {title}
        </Box>
      </Flex>
    </Flex>
  );
};

export default PrizeTitle;
