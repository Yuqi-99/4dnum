import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  drawList: [],
  selectedDrawDate: '',
  value: [],
};

export const resultSlice = createSlice({
  name: 'result',
  initialState,
  reducers: {
    setDrawList: (state, action) => {
      state.drawList = action.payload;
    },
    setSelectedDrawDate: (state, action) => {
      state.selectedDrawDate = action.payload;
    },
    setResults: (state, action) => {
      state.value = action.payload;
    },
  },
});

export const {setDrawList, setSelectedDrawDate, setResults} =
  resultSlice.actions;

export default resultSlice.reducer;
