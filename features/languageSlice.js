import {createSlice} from '@reduxjs/toolkit';
import Cookie from 'react-native-cookie';

import {DEFAULT_LANGUAGE} from '../utils/constants';

const initialState = {
  language: 'th',
};

export const languageSlice = createSlice({
  name: 'language',
  initialState,
  reducers: {
    setLanguage: (state, action) => {
      state.language = action.payload;
    },
  },
});

export const {setLanguage} = languageSlice.actions;

export default languageSlice.reducer;
